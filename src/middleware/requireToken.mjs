import verifyToken from "../utils/verifyToken.mjs";

const requireToken = (req, res, next) => {
  try {
    //const token = req.cookies.token;
    const token = req.headers.authorization;
    if (token == null) return res.sendStatus(401);
    const user = verifyToken(token);
    req.user = user;
    next();
  } catch (err) {
    console.error("Error happened (requireToken)", err);
    next();
  }
};

export default requireToken;
