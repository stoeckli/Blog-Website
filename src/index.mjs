import express from "express";
import todoRouter from "./routes/blog.routes.mjs";
import users from "./routes/user.routes.mjs";
import connect from "./utils/db.mjs";
import cors from "cors";
import cookieParser from "cookie-parser";
import dotenv from "dotenv";

dotenv.config();

connect();

const app = express();
const port = process.env.PORT || 3001;

import { createServer } from "http";
import { Server } from "socket.io";

const httpServer = createServer(app);
const io = new Server(httpServer, {
  cors: { origin: "*" },
});

io.on(`connection`, (socket) => {
  socket.on(`message`, ({ showPushNotification, blogId }) => {
    socket.broadcast.emit(`message`, {
      blogId,
    });
  });
});

app.use(express.json());
app.use(cookieParser());

app.get("/", (req, res) => {
  res.send("Hello");
});

httpServer.listen(port, () => {
  console.log(`Blog listening at ${process.env.WEBSITE_URL}`);
});

let corsOptions = {
  origin: ["https://ibaw-blog-frontend.herokuapp.com", "http://localhost:3000"],
  credentials: true,
};

app.use(cors(corsOptions));

app.use("/", todoRouter);
app.use("/userAccount/", users);
