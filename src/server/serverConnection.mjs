import mongodb from "mongodb";
const { MongoClient } = mongodb;

export const url = `mongodb://localhost/blog`;

let db;

async function usersList() {
  const users = await db.collection("users").find({}).toArray();
  return users;
}

export const connectToDb = async (req, res) => {
  const client = new MongoClient(url, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  });
  await client.connect();
  db = client.db();
  const entries = await db.collection("blogEntries").find({}).toArray();
};

export async function entryList() {
  const client = new MongoClient(url, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  });
  await client.connect();
  db = client.db();
  const entries = await db.collection("blogEntries").find({}).toArray();
  return entries;
}
