import jwt from "jsonwebtoken";

const verifyToken = (token) => {
  return jwt.verify(token, process.env.TOKEN_SECRET, (err, user) => {
    if (err) return err;
    return user;
  });
};

export default verifyToken;
