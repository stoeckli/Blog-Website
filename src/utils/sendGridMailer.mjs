import sgMail from "@sendgrid/mail";
import dotenv from "dotenv";
import Users from "../models/User.mjs";

let users = Users.find();

const SENDGRID_API_KEY =
  "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";

const USER = "cedric.stoeckli@gmail.com";

sgMail.setApiKey(SENDGRID_API_KEY);

const msg = {
  to: "cedric.stoeckli@gmail.com",
  from: "cedric.stoeckli@gmail.com", // Use the email address or domain you verified above
  subject: "Sending with Twilio SendGrid is Fun",
  text: "and easy to do anywhere, even with Node.js",
  html: "<strong>and easy to do anywhere, even with Node.js</strong>",
};

export async function sendMail(entry) {
  try {
    for await (const test of users) {
      const mailOptions = {
        from: "Cedric Stoeckli <cedric.stoeckli@gmail.com>",
        to: test.email,
        subject: "Blog",
        text: `A new post has been created, please take a look if you are interested:<br>${websiteUrl}/blog/${entryId}`,
        html: `A new post has been created, please take a look if you are interested:<br>${websiteUrl}/blog/${entryId}`,
      };
      const result = await sgMail.send(mailOptions);
    }
    return result;
  } catch (error) {
    console.error(error);

    if (error.response) {
      console.error(error.response.body);
    }
  }
}

export async function sendWelcomeMail(userData) {
  try {
    const mailOptions = {
      from: "Cedric Stoeckli <cedric.stoeckli@gmail.com>",
      to: userData.email,
      subject: "Blog",
      text: `Welcome ${userData.username}<br><br>You have succesfully signed up your user. Enjoy reading the blog entries`,
      html: `Welcome ${userData.username}<br><br>You have succesfully signed up your user. Enjoy reading the blog entries`,
    };
    const result = await sgMail.send(mailOptions);

    return result;
  } catch (error) {
    console.error(error);

    if (error.response) {
      console.error(error.response.body);
    }
  }
}

export async function sendResetMail(recipient) {
  try {
    const mailOptions = {
      from: `Cedric Stoeckli <${USER}>`,
      to: recipient.email,
      subject: "Reset password",
      html: `Click to the following link to reset your password: https://ibaw-blog-frontend.herokuapp.com/reset/${recipient._id}`,
    };
    const result = await sgMail.send(mailOptions);

    return result;
  } catch (error) {
    console.error(error);

    if (error.response) {
      console.error(error.response.body);
    }
  }
}
