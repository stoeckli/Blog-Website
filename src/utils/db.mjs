import mongoose from "mongoose";
import dotenv from "dotenv";

dotenv.config();

const mongoUri = process.env.MONGO_URI;

export default function connect() {
  try {
    return mongoose.connect(mongoUri);
  } catch (e) {
    console.error(e);
  }
}
