import nodemailer from "nodemailer";
import Users from "../models/User.mjs";
import googleapis from "googleapis";
const { google } = googleapis;

let users = Users.find();

const CLIENT_ID = process.env.CLIENT_ID;
const CLIENT_SECRET = process.env.CLIENT_SECRET;
const REDIRECT_URI = process.env.REDIRECT_URI;
const REFRESH_TOKEN = process.env.REFRESH_TOKEN;

export const oAuth2Client = new google.auth.OAuth2(
  process.env.CLIENT_ID,
  process.env.CLIENT_SECRET,
  process.env.REDIRECT_URI
);

oAuth2Client.setCredentials({ refresh_token: process.env.REFRESH_TOKEN });

export async function sendMail(entry) {
  try {
    const accessToken = await oAuth2Client.getAccessToken();
    const transport = nodemailer.createTransport({
      service: "gmail",
      auth: {
        type: "OAuth2",
        user: "cedric.stoeckli@gmail.com",
        clientId: process.env.CLIENT_ID,
        clientSecret: process.env.CLIENT_SECRET,
        refreshToken: process.env.REFRESH_TOKEN,
        accessToken: accessToken,
      },
    });
    let entryId = entry.id;

    for await (const test of users) {
      const mailOptions = {
        from: "Cedric Stoeckli <cedric.stoeckli@gmail.com>",
        to: test.email,
        subject: "Blog",
        text: `A new post has been created, please take a look if you are interested:<br>${websiteUrl}/blog/${entryId}`,
        html: `A new post has been created, please take a look if you are interested:<br>${websiteUrl}/blog/${entryId}`,
      };
      const result = transport.sendMail(mailOptions);
    }
    return result;
  } catch (error) {
    return error;
  }
}

export async function resetInfodMail(user) {
  try {
    const accessToken = await oAuth2Client.getAccessToken();
    const transport = nodemailer.createTransport({
      service: "gmail",
      auth: {
        type: "OAuth2",
        user: "cedric.stoeckli@gmail.com",
        clientId: process.env.CLIENT_ID,
        clientSecret: process.env.CLIENT_SECRET,
        refreshToken: process.env.REFRESH_TOKEN,
        accessToken: accessToken,
      },
    });

    const mailOptions = {
      from: "Cedric Stoeckli <cedric.stoeckli@gmail.com>",
      to: user.email,
      subject: "Blog",
      text: `A new post has been created, please take a look if you are interested:<br>${websiteUrl}/blog/${user._id}`,
      html: `A new post has been created, please take a look if you are interested:<br>${websiteUrl}/blog/${entryId}`,
    };
    const result = transport.sendMail(mailOptions);
    return result;
  } catch (error) {
    return error;
  }
}
