import nodemailer from "nodemailer";
import googleapis, { Auth } from "googleapis";
import { auth } from "google-auth-library";
const { google } = googleapis;
const OAuth2 = google.auth.OAuth2;
import dotenv from "dotenv";

const CLIENT_ID =
  "370725190802-vem1pulavdoh17sld85n57hf1cgof33r.apps.googleusercontent.com";
const CLIENT_SECRET = "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";
const REDIRECT_URI = "https://developers.google.com/oauthplayground";
const REFRESH_TOKEN =
  "1//04uryXS1pmyaWCgYIARAAGAQSNwF-L9Ir8P7zqLNDvvfcWPRgnDmjWuHugPTSJ5U4bNbzIfhgtsKntRL-X9gJEdZjbuGdLQSn4PE";
const USER = "cedric.stoeckli@gmail.com";

const OAuth2_client = new OAuth2(CLIENT_ID, CLIENT_SECRET);
OAuth2_client.setCredentials({ refresh_token: REFRESH_TOKEN });

export default function send_mail(recipient) {
  const accessToken = OAuth2_client.getAccessToken();

  const transport = nodemailer.createTransport({
    service: "gmail",
    auth: {
      type: "OAuth2",
      user: "cedric.stoeckli@gmail.com",
      clientId: CLIENT_ID,
      clientSecret: CLIENT_SECRET,
      refreshToken: REFRESH_TOKEN,
      accessToken: accessToken,
    },
  });
  const mail_options = {
    from: `Cedric Stoeckli <${USER}>`,
    to: recipient.email,
    subject: "Reset password",
    html: `Click to the following link to reset your password: https://ibaw-blog-frontend.herokuapp.com/reset/${recipient._id}`,
  };

  transport.sendMail(mail_options, function (error, result) {
    if (error) {
      console.log(`Error: ${error}`);
    } else {
      console.log(`Success: ${result}`);
    }
    transport.close();
  });
}
