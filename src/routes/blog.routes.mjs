import express from "express";
import jwt from "jsonwebtoken";

import {
  getBlogCategoryEntries,
  getBlogEntries,
  getBlogEntry,
  createBlogEntry,
  updateBlogEntry,
  deleteBlogEntry,
  createComment,
  deleteComment,
  getPersonalBlogEntries,
} from "../controllers/blogController.mjs";
import requireToken from "../middleware/requireToken.mjs";

const router = express.Router();

router.get("/blog/category/:categoryname", getBlogCategoryEntries);
router.get("/blog", getBlogEntries);
router.get("/blog/:id", getBlogEntry);
router.post("/blog", requireToken, createBlogEntry);
router.post("/blog/:id", requireToken, createComment);
router.put("/blog/:id", requireToken, updateBlogEntry);
router.delete("/blog/:id", requireToken, deleteBlogEntry);

router.delete("/blog/:id/:commentid", requireToken, deleteComment);
router.get("/myblogs/:id", getPersonalBlogEntries);

export default router;
