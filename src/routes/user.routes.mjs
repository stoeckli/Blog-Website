import express from "express";
import jwt from "jsonwebtoken";

import {
  createUser,
  updateUser,
  deleteUser,
  getWebToken,
  verifyUser,
  logoutUser,
  forgotPassword,
  resetPassword,
  getUserId,
  changeUserCredentials,
  getProfileImageUrl,
} from "../controllers/userController.mjs";
import requireToken from "../middleware/requireToken.mjs";

const userRoutes = express.Router();

userRoutes.post("/login", getWebToken);
userRoutes.post("/", createUser);
userRoutes.get("/verify", verifyUser);
userRoutes.get("/logout", logoutUser);

userRoutes.put("/:id", requireToken, updateUser);
userRoutes.delete("/:id", requireToken, deleteUser);
userRoutes.post("/forgot", forgotPassword);
userRoutes.post("/reset/:id", resetPassword);
userRoutes.post("/userId", getUserId);
userRoutes.post("/changeUserCredentials/:id", changeUserCredentials);
userRoutes.post("/imageUrl", requireToken, getProfileImageUrl);

export default userRoutes;
