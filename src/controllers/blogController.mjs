import Joi from "joi";
import jwt from "jsonwebtoken";
import Blogpost from "../models/Blogpost.mjs";
import Comment from "../models/Comment.mjs";
import mongoose from "mongoose";

export const getBlogEntries = async (req, res, next) => {
  try {
    const entries = await Blogpost.find();
    const { id, title, author } = req.query;
    let filteredEntries;
    let identificationNumbers;
    let queryStrings = false;
    if (title) {
      queryStrings = true;
      filteredEntries = entries.filter((entry) => {
        return entry.title === title;
      });
    }
    if (id) {
      queryStrings = true;
      if (filteredEntries) {
        filteredEntries = filteredEntries.filter(
          (entry) => entry.id === parseInt(id)
        );
      } else {
        filteredEntries = entries.filter((entry) => {
          if (entry.id === parseInt(id)) {
            return entry.id === parseInt(id);
          }
        });
      }
    }
    if (author) {
      queryStrings = true;
      if (filteredEntries) {
        filteredEntries = filteredEntries.filter(
          (entry) => entry.author === author
        );
      } else {
        filteredEntries = entries.filter((entry) => {
          if (entry.author === author) {
            return entry.author === author;
          }
        });
      }
    }
    if (undefined !== filteredEntries && filteredEntries.length > 0) {
      res.json(filteredEntries);
    } else if (queryStrings) {
      res
        .status(404)
        .send("The blog with the given query parameters was not found");
    } else {
      res.send(entries);
    }
  } catch (err) {
    console.error("Error happened (getBlogEntries)", err);
    next();
  }
};

export const getBlogEntry = async (req, res, next) => {
  try {
    const blog = await Blogpost.findOne({
      _id: mongoose.Types.ObjectId(req.params.id),
    });
    if (!blog) {
      return res.send({
        success: false,
        message: "The blog with the given ID was not found",
      });
    } else {
      return res.send({
        success: true,
        message: "The blog with the given ID has been found",
        blog: blog,
      });
    }
  } catch (err) {
    console.error("Error happened (getBlogEntry)", err);
    next();
  }
};

export const getBlogCategoryEntries = async (req, res, next) => {
  try {
    const blog = await Blogpost.find({ category: req.params.categoryname });
    if (blog.length > 0) {
      res.send(blog);
    } else {
      return res
        .status(404)
        .send("The blog with the given category was not found");
    }
  } catch (err) {
    console.error("Error happened (getBlogCategoryEntries))", err);
    next();
  }
};

export const createComment = async (req, res, next) => {
  try {
    const entries = await Blogpost.find();
    const blog = await Blogpost.findOne({
      _id: mongoose.Types.ObjectId(req.params.id),
    });

    if (!blog) {
      return res.status(404).send("The blog with the given ID was not found");
    } else {
      const entry = {
        author: req.user.user._id,
        text: req.body.text,
      };

      await Blogpost.updateOne(
        { _id: mongoose.Types.ObjectId(req.params.id) },
        {
          $push: {
            comments: await Comment.create(entry),
          },
        }
      );

      res.send(
        await Blogpost.findOne({
          _id: mongoose.Types.ObjectId(req.params.id),
        })
      );
    }
  } catch (err) {
    console.error("Error happened (createComment)", err);
    next();
  }
};

export const createBlogEntry = async (req, res, next) => {
  try {
    try {
      const blogPost = await new Blogpost({
        category: req.body.category,
        title: req.body.title,
        content: req.body.content,
        imageUrl: req.body.imageUrl,
        author: req.user.user._id,
      });

      blogPost.save(function (err, post) {
        return res.send({
          success: true,
          message: "Blog article has been created",
          blog: post,
        });
      });
    } catch (e) {
      return res.send("Error while creating blogpost");
    }
  } catch (err) {
    console.error("Error happened (createBlogEntry)", err);
    next();
  }
};

export const updateBlogEntry = async (req, res, next) => {
  try {
    const blog = await Blogpost.findOne({
      _id: mongoose.Types.ObjectId(req.params.id),
    });
    if (!blog) {
      return res.status(404).send("The blog with the given ID was not found");
    }

    if (req.user.user.username === blog.author.username) {
      const schema = {
        title: Joi.string().min(3),
        content: Joi.string().min(10),
        category: Joi.string(),
        imageUrl: Joi,
      };
      const result = Joi.validate(req.body, schema);
      if (result.error) {
        return res.status(400).send(result.error.details[0].message);
      }

      await Blogpost.findOneAndUpdate(
        { _id: mongoose.Types.ObjectId(req.params.id) },
        { date: todaysDate() },
        {
          new: true,
        }
      );

      return res.send({
        success: true,
        message: "Blog entry has been updated",
        blogs: await Blogpost.findOneAndUpdate(
          { _id: mongoose.Types.ObjectId(req.params.id) },
          req.body,
          {
            new: true,
          }
        ),
      });
    } else {
      return res
        .status(404)
        .send("No permission to adapt a foreign blog entry");
    }
  } catch (err) {
    console.error("Error happened (updateBlogEntry)", err);
    next();
  }
};

export const deleteBlogEntry = async (req, res, next) => {
  try {
    const blog = await Blogpost.findOne({
      _id: mongoose.Types.ObjectId(req.params.id),
    });
    if (!blog) {
      return res.status(404).send("The course with the given ID was not found");
    }
    if (req.user.user.username === blog.author.username) {
      await Blogpost.deleteOne({
        _id: mongoose.Types.ObjectId(req.params.id),
      });
      return res.send({
        success: true,
        message: "Blog entity has been deleted",
        blogs: blog,
      });
      //res.send(blog);
    } else {
      return res
        .status(404)
        .send("No permission to delete a foreign blog entry");
    }
  } catch (err) {
    console.error("Error happened (deleteBlogEntry)", err);
    next();
  }
};

export const deleteComment = async (req, res, next) => {
  try {
    const blog = await Blogpost.findOne({
      _id: mongoose.Types.ObjectId(req.params.id),
    });

    if (!blog) {
      return res.status(404).send("The blog with the given ID was not found");
    }
    const comments = await blog.comments.filter((currentComment) => {
      return currentComment._id != req.params.commentid;
    });

    const commentToDelete = await blog.comments.filter((currentComment) => {
      return currentComment._id == req.params.commentid;
    });
    const commentUserName = commentToDelete.find(
      (element) => element._id == req.params.commentid
    );

    if (req.user.user.username == commentUserName.author.username) {
      await Blogpost.findOneAndUpdate(
        {
          _id: mongoose.Types.ObjectId(req.params.id),
        },
        {
          comments,
        },
        {
          new: true,
        }
      );

      res.send(
        await Blogpost.findOneAndUpdate(
          { _id: mongoose.Types.ObjectId(req.params.id) },
          req.body,
          {
            new: true,
          }
        )
      );
    } else {
      return res.status(404).send("No permission to delete a foreign comment");
    }
  } catch (err) {
    console.error("Error happened (deleteComment)", err);
    next();
  }
};

function todaysDate() {
  try {
    const dt = new Date();
    let month = "" + (dt.getMonth() + 1);
    let day = "" + dt.getDate();
    let year = dt.getFullYear();
    let hours = dt.getHours();
    let minutes = dt.getMinutes();
    let seconds = dt.getSeconds();

    month = (month < 10 ? "0" : "") + month;
    day = (day < 10 ? "0" : "") + day;

    let time = `${hours}:${minutes}:${seconds}`;
    let date = [year, month, day].join("-");
    return [date, time].join(" - ");
  } catch (err) {
    console.error("Error happened", err);
  }
}

export const verifyToken = (req, res, next) => {
  try {
    //get auth header value
    const bearerHeader = req.headers["authorization"];
    if (typeof bearerHeader !== "undefined") {
      const bearer = bearerHeader.split(" ");
      const bearerToken = bearer[1];
      req.token = bearerToken;
      next();
    } else {
      res.sendStatus(403);
    }
  } catch (err) {
    console.error("Error happened", err);
    next();
  }
};

export const getPersonalBlogEntries = async (req, res, next) => {
  try {
    const test = await Blogpost.find().populate("author");
    const filteredEntries = test.filter((filtered) => {
      if (filtered.author) {
        return (
          JSON.stringify(filtered.author._id) ==
          JSON.stringify(mongoose.Types.ObjectId(req.params.id))
        );
      }
    });
    if (filteredEntries.length > 0) {
      return res.send({
        success: true,
        message: "Personal blog entries have been found",
        count: filteredEntries.length,
        blogs: filteredEntries,
      });
    } else {
      return res.send({
        success: false,
        message: "No personal blog entries have been found",
        count: filteredEntries.length,
        blogs: filteredEntries,
      });
    }
  } catch (err) {
    console.error("Error happened (getPersonalBlogEntries)", err);
    next();
  }
};
