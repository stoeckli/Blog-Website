"use strict";

import User from "../models/User.mjs";
import UserSession from "../models/UserSession.mjs";
import mongoose from "mongoose";
import * as mail from "../utils/mailer.mjs";
import verifyToken from "../utils/verifyToken.mjs";
import {
  sendMail,
  sendWelcomeMail,
  sendResetMail,
} from "../utils/sendGridMailer.mjs";
import jwt from "jsonwebtoken";
import Joi from "joi";

export const createUser = async (req, res, next) => {
  try {
    const newUserData = {
      username: req.body.username,
      email: req.body.email,
    };
    const previousEmail = await User.findOne({ email: newUserData.email });
    const previousUsername = await User.findOne({
      username: req.body.username,
    });
    if (previousEmail) {
      return res.send({
        success: false,
        message:
          "Error: Email already exists, please choose a new email address",
      });
      return res.status(404).send("Error: Account already exists");
    } else if (previousUsername) {
      return res.send({
        success: false,
        message: "Error: Username already exists, please choose a new username",
      });
    } else {
      const newUser = await new User();
      newUser.username = req.body.username;
      newUser.email = req.body.email;
      newUser.isDeleted = false;
      newUser.password = await newUser.generateHash(req.body.password);
      newUser.save(function (err, user) {
        res.send({
          success: true,
          message: "User has been created",
          user: user,
        });
      });
    }

    sendWelcomeMail(newUserData)
      .then((result) => console.log("Email sent", result))
      .catch((error) => console.log(error.message));
  } catch (err) {
    console.error("Error happened (createUser)", err);
    next();
  }
};

export const getUserId = async (req, res, next) => {
  try {
    const user = await User.findOne({
      username: req.body.username,
    });
    if (!user) {
      return res.send({
        success: false,
        message: "Error: The given user was not found",
      });
    }
    return res.send({
      success: true,
      message: "User was found",
      userId: user._id,
    });
  } catch (err) {
    console.error("Error happened (getUserId)", err);
    next();
  }
};

export const changeUserCredentials = async (req, res, next) => {
  try {
    const credentials = {
      username: req.body.username,
      password: req.body.password,
      oldPassword: req.body.oldPassword,
      confirmedPassword: req.body.confirmedPassword,
    };
    const updateUser = await User.findOne({
      _id: mongoose.Types.ObjectId(req.params.id),
    });
    if (!updateUser) {
      return res.status(404).send("The user with the given ID was not found");
    }

    if (req.body.changePassword) {
      const schema = {
        username: Joi.string().required(),
        password: Joi.string().min(6).required(),
        oldPassword: Joi.string().min(6).required(),
        confirmedPassword: Joi.string().min(6).required(),
      };
      const result = Joi.validate(credentials, schema);
      if (result.error) {
        console.log(result.error.details[0].message);
        return res.status(400).send(result.error.details[0].message);
      }
      const oldPwInput = await updateUser.validPassword(
        credentials.oldPassword
      );
      if (oldPwInput) {
        updateUser.password = await updateUser.generateHash(req.body.password);
        updateUser.username = req.body.username;
        updateUser.save(function (err, newUserInformation) {
          return res.send({
            success: true,
            message: "Profile Informations have been changed",
            newUsername: req.body.username,
          });
        });
      } else {
        return res.send({
          success: false,
          message: "old password does not match",
        });
      }
    } else {
      updateUser.username = req.body.username;
      updateUser.save(function (err, newUserInformation) {
        return res.send({
          success: true,
          message: "Profile Informations have been changed",
          newUsername: req.body.username,
        });
      });
    }
  } catch (err) {
    console.error("Error happened (changeUserCredentials)", err);
    next();
  }
};

export const updateUser = async (req, res, next) => {
  try {
    const updateUser = await User.findOne({
      _id: mongoose.Types.ObjectId(req.params.id),
    });
    if (!updateUser) {
      return res.status(404).send("The user with the given ID was not found");
    }

    const schema = {
      imageUrl: Joi.string().min(10),
    };
    const result = Joi.validate(req.body, schema);
    if (result.error) {
      return res.status(400).send(result.error.details[0].message);
    }

    updateUser.imageUrl = req.body.imageUrl;
    updateUser.save(function (err, newUserInformation) {
      return res.send({
        success: true,
        message: "Profile Information have been changed",
      });
    });
  } catch (err) {
    console.error("Error happened (updateUser)", err);
    next();
  }
};

export const deleteUser = async (req, res, next) => {
  try {
    const userEntry = await User.findOne({
      _id: mongoose.Types.ObjectId(req.params.id),
    });
    if (!userEntry) {
      return res.status(404).send("The user with the given ID was not found");
    }
    if (req.user.user.username === userEntry.username) {
      await User.deleteOne({
        _id: mongoose.Types.ObjectId(req.params.id),
      });
      return res.send({
        success: true,
        message: "User has been deleted",
      });
      //return res.send(userEntry);
    } else {
      return res.status(404).send("No permission to delete a foreign user");
    }
  } catch (err) {
    console.error("Error happened (deleteUser)", err);
    next();
  }
};

export const getWebToken = async (req, res, next) => {
  try {
    const schema = {
      email: Joi.string().min(5).required(),
      password: Joi.string().min(6).required(),
    };
    const result = Joi.validate(req.body, schema);
    if (result.error) {
      return res.status(400).send(result.error.details[0].message);
    }
    User.find({ email: req.body.email }, (err, users) => {
      if (err) {
        return res.send({
          success: false,
          message: "Error: server error",
        });
      }
      if (users.length != 1) {
        return res.send({
          success: false,
          message: "Error: Invalid e-mail address or password",
        });
      }
      const user = users[0];
      if (!user.validPassword(req.body.password)) {
        return res.send({
          success: false,
          message: "Error: Invalid e-mail address or password",
        });
      }

      //Otherwise correct user
      jwt.sign(
        { user },
        process.env.TOKEN_SECRET,
        { expiresIn: "4h" },
        (err, token) => {
          const userSession = new UserSession();
          userSession.userId = user._id;
          userSession.token = token;
          userSession.save((err, doc) => {
            if (err) {
              return res.send({
                success: false,
                message: "Error: Server error",
              });
            }
            // send cookie
            res.cookie("token", token, {
              sameSite: "strict",
              httpOnly: true,
            });
            return res.send({
              success: true,
              message: "Valid sign in",
              username: user.username,
              token: token,
            });
          });
        }
      );
    });
  } catch (err) {
    console.error("Error happened (getWebToken)", err);
    next();
  }
};

export const verifyUser = async (req, res, next) => {
  try {
    //const token = req.cookies.token;
    const token = req.headers.authorization;

    try {
      //const user = jwt.verify(token, process.env.TOKEN_SECRET);
      const user = verifyToken(token);
    } catch (e) {
      await UserSession.findOneAndUpdate(
        { isDeleted: false, token: token },
        { isDeleted: true }
      );
      return res.send({ error: "Token invalid or expired!" });
    }

    UserSession.find({ isDeleted: false, token: token }, (err, sessions) => {
      if (err) {
        return res.send({
          success: false,
          message: "Error: Server error",
        });
      }
      if (sessions.length != 1) {
        return res.send({
          success: false,
          message: "Error: Invalid token",
        });
      } else {
        User.findOne(
          {
            _id: mongoose.Types.ObjectId(sessions[0].userId),
          },
          (err, user) => {
            if (err)
              return res.send({
                success: true,
                message: "Good",
                imageUrl: "no image Url found",
              });
            else {
              return res.send({
                success: true,
                message: "Good",
                imageUrl: user.imageUrl,
              });
            }
          }
        );
      }
    });
  } catch (err) {
    console.error("Error happened (verifyUser)", err);
    next();
  }
};

export const logoutUser = async (req, res, next) => {
  try {
    const { query } = req;
    const { token } = query;

    UserSession.findOneAndUpdate(
      { token: token, isDeleted: false },
      {
        $set: { isDeleted: true },
      },
      null,
      (err, session) => {
        if (err) {
          return res.send({
            success: false,
            message: "Error: Server error",
          });
        }
        return res.send({
          success: true,
          message: "Good",
        });
      }
    );
  } catch (err) {
    console.error("Error happened (logoutUser)", err);
    next();
  }
};

export const forgotPassword = async (req, res, next) => {
  try {
    const newUserData = {
      email: req.body.email,
    };
    const emailExists = await User.findOne({ email: newUserData.email });

    if (!emailExists) {
      return res.send({
        success: false,
        message:
          "Please check your inbox, we have sent a email if this was a valid user account",
        email: newUserData.email,
      });
    } else {
      sendResetMail(emailExists);

      return res.send({
        success: true,
        message:
          "Please check your inbox, we have sent a email if this was a valid user account",
        email: newUserData.email,
      });
    }

    res.send(newUserData);
  } catch (err) {
    console.error("Error happened (forgotPassword)", err);
    next();
  }
};

export const resetPassword = async (req, res, next) => {
  try {
    const user = await User.findOne({
      _id: mongoose.Types.ObjectId(req.params.id),
    });
    if (!user) {
      return res.status(404).send("The user with the given ID was not found");
    }

    const schema = {
      password: Joi.string().min(6).required(),
    };
    const result = Joi.validate(req.body, schema);
    if (result.error) {
      return res.status(400).send(result.error.details[0].message);
    }

    const updateUser = await User.findOne({
      _id: mongoose.Types.ObjectId(req.params.id),
    });
    updateUser.password = await updateUser.generateHash(req.body.password);
    updateUser.save(function (err, newUserInformation) {
      return res.send({
        success: true,
        message: "Password has been changed",
      });
    });
  } catch (err) {
    console.error("Error happened (resetPassword)", err);
    next();
  }
};

export const getProfileImageUrl = async (req, res, next) => {
  try {
    const user = await User.findOne({ username: req.body.username });
    if (user) {
      return res.send({
        success: true,
        message: "Good",
        imageUrl: user.imageUrl,
      });
    } else {
      return res.send({
        success: false,
        message: "Error: The given user was not found",
      });
    }
  } catch (err) {
    console.error("Error happened (getProfileImageUrl)", err);
    next();
  }
};
