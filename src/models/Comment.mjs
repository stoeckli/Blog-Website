import mongoose from "mongoose";
import mongooseAutopopulate from "mongoose-autopopulate";

const { Schema } = mongoose;

const commentSchema = new Schema(
  {
    text: {
      type: String,
      required: true,
    },
    author: {
      type: Schema.Types.ObjectId,
      ref: "User",
      required: true,
      autopopulate: true,
    },
  },
  { timestamps: true }
);

commentSchema.plugin(mongooseAutopopulate);

export default mongoose.model(`Comment`, commentSchema, "comment");
