import mongoose from "mongoose";
import mongooseAutopopulate from "mongoose-autopopulate";

const { Schema } = mongoose;

const blogpostSchema = new Schema(
  {
    title: {
      type: String,
      required: true,
    },
    content: {
      type: String,
      required: true,
    },
    category: {
      type: String,
      required: true,
    },
    imageUrl: {
      type: String,
      required: false,
    },
    author: {
      type: Schema.Types.ObjectId,
      ref: "User",
      required: true,
      autopopulate: true,
    },
    comments: [
      {
        type: mongoose.Types.ObjectId,
        ref: "Comment",
        autopopulate: true,
      },
    ],
  },
  { timestamps: true }
);

blogpostSchema.plugin(mongooseAutopopulate);

export default mongoose.model(`Blogpost`, blogpostSchema, "blogpost");
