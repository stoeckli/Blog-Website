import mongoose from "mongoose";

const { Schema } = mongoose;

const userSessionSchema = new Schema({
  userId: {
    type: String,
    default: "",
  },
  timestamp: {
    type: Date,
    default: Date.now(),
  },
  isDeleted: {
    type: Boolean,
    default: false,
  },
  token: {
    type: String,
    default: "",
  },
});

export default mongoose.model(`UserSession`, userSessionSchema, "userSession");
